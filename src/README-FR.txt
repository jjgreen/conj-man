FR-Man
------

This is a set of Unix manpages of French conjugations
to install, just copy *.7 to /usr/local/man/man7/

For more details on the package see

  http://soliton.vm.bytemark.co.uk/pub/jjg/en/code/conj-man/

The list of French infinitives and their English translations 
is a modified version of the list by Will Dudziak
 
  http://www.dudziak.com/verbs.php

The conjugations are from the Verbiste program by Pierre Sarrazin,

  http://sarrazip.com/dev/verbiste.html

All files in the package are released under the GNU General Public
License (GPL), see the file COPYING for details.

J.J.Green 2015 (j.j.green@gmx.com)

