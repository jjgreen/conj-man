IT-Man
------

This is a set of Unix manpages of Italian conjugations
to install, just copy *.7 to /usr/local/man/man7/

For more details on the package see

  http://soliton.vm.bytemark.co.uk/pub/jjg/en/code/conj-man/

The conjugations are from the Verbiste program by Pierre Sarrazin,

  http://sarrazip.com/dev/verbiste.html

All files in the package are released under the GNU General Public
License (GPL), see the file COPYING for details.

J.J.Green 2015 (j.j.green@gmx.com)

