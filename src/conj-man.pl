#! /usr/bin/perl
#
# icman.pl
# Copyright (c) 2010, 2014, J.J. Green
#
# Reads lines from stdin looking like
#
#   absoudre        ; to absolve
#   abstenir (s')   ; to abstain
#
# creating a man-page of the conjugation for each verb.
#
# The french-conjugator program (part of verbiste) must
# be on the path.

use strict;

use POSIX;
use IPC::Open2;
use IO::Handle;

# whether or not to align the verbs using the tbl
# decimal-point alignment mechanism, depends on your
# taste

my $align = 0;

if (scalar(@ARGV) != 4)
{
    print "usage: perl conj-man.pl <lang> <fcpath> <ver> <date>\n";
    print "where\n";
    print "  <lang>   language, one of 'it' or 'fr'\n";
    print "  <fcpath> the path to the french-conjugator program\n";
    print "  <ver>    the package version\n";
    exit 1;
}

my ($lang, $fcpath, $fcver, $fcdate) = @ARGV;

my $cmd = "$fcpath --pronouns --lang=$lang";
my ($fci, $fco);
my $fcpid = open2($fco, $fci, $cmd) or die "open2 $cmd";

binmode $fco,  ':utf8';
binmode $fci,  ':utf8';
binmode STDIN, ':utf8';

my @pernos = ('1s', '2s', '3s', '1p', '2p', '3p');

while (my $line = <STDIN>)
{
    chomp $line;

    next if $line =~ /^\#/o; 
    next if $line =~ /^\w*$/o;

    my ($fr, $en) = split(';', $line);

    $en = trim($en);
    $fr = trim($fr);

    my ($inf, $pro);

    if (my @ms = ($fr =~ /(\w+)(\s+\((.*)\))?/o))
    {
	$inf = shift @ms;

	if (@ms)
	{
	    shift @ms;
	    $pro = shift @ms;
	}
    }
    else
    {
	die "bad line \"$fr\" in input\n";
    }
    
    $inf = trim($inf);

    my $conj = conjugate($inf, $lang);
    
    unless ($conj)
    {
	printf("%s (%s) not found\n", $inf, $en);
	next;
    }
    
    manpage($lang, $inf, $pro, $conj, $en, 7, $align, $fcdate);
}

# trim whitespace fore and aft

sub trim
{
    my $st = shift;

    $st =~ s/^\s+//;
    $st =~ s/\s+$//;

    return $st;
}

# lang : 'fr' or 'it'
# inf  : infinitive
# pro  : reflexive pronoun, may be undefined
# conj : conjugation hash
# eng  : corresponding English infinitive (need not be English!)
# sect : section of (Unix) manual

sub manpage
{
    my ($lang, $inf, $pro, $conj, $eng, $sect, $align, $date) = @_;

    my ($asym, $aspec);

    if ($align)
    {
	$asym  = '\&';
	$aspec = "n n n n.\n";
    }
    else
    {
	$asym  = '';
	$aspec = "L L L L.\n";
    }

    my $prostr = ($pro ? " ($pro)" : "");

    my $man = "$inf.$sect";

    open MAN, "> $man" or die "failed to open $man";

    binmode MAN, ':encoding(utf8)';

    printf MAN ".TH %s %i \"%s\" \"%s-Man %s\"\n", 
               uc($inf), $sect, $date, uc($lang), $fcver;
    printf MAN ".SH NAME\n";
    printf MAN "%s%s \\- %s\n", $inf, $prostr, $eng;
    printf MAN "\n";

    print MAN ".SH CONJUGATION\n";

    print MAN ".P\n";

    # table header
    print MAN 
	".TS\n".
	"center tab(%);\n".
	"Lb  Lb  Lb  Lb\n".
	"L   L   L   L.\n";

    # infinitive header and row
    print MAN 
	"inf.\\ pres. % % %\n".
	"$inf % % %\n".
	"% % % %\n";
	
    # row 2 header
    print MAN 
	".T&\n", 
	"Lb Lb Lb Lb\n", 
	$aspec, 
	join(' % ', ('ind.\ pres.', 
		    'ind.\ imperf.', 
		    'ind.\ fut.', 
		    'ind.\ past')), 
	"\n";

    foreach my $perno (@pernos) 
    {
	my @strs = ();

	foreach my $tense ('indpre', 'indimp', 'indfut', 'indpas')
	{
	    if ($conj->{$tense}->{$perno}->{form})
	    {
		my $str = join($asym, 
			       (
				$conj->{$tense}->{$perno}->{pronoun}, 
				$conj->{$tense}->{$perno}->{form}
			       ));

		push @strs, $str;
	    }
	    else
	    {
		push @strs, ' ';
	    }
	}

	print MAN join(' % ', @strs), "\n";
    }

    printf MAN "%% %% %% %%\n";
    
    # row 3 header
    print MAN 
	".T&\n", 
	"Lb Lb Lb Lb\n", 
	$aspec, 
	join(' % ', ('cond.\ pres.', 
		    'subj.\ pres.', 
		    'subj.\ imperf.', 
		    '')), 
	"\n";

    foreach my $perno (@pernos) 
    {
	my @strs = ();

	foreach my $tense ('conpre', 'subpre', 'subimp')
	{
	    if ($conj->{$tense}->{$perno}->{form})
	    {
		my $str = 
		    join($asym, 
			 (
			  $conj->{$tense}->{$perno}->{pronoun}, 
			  $conj->{$tense}->{$perno}->{form}
			 )
		    );

		push @strs, $str;
	    }
	    else
	    {
		push @strs, ' ';
	    }
	}

	print MAN join(' % ', @strs), "\n";
    }

    print MAN "% % % %\n";

    # row 4
    
    if ($lang eq 'fr')
    {
	print MAN 
	    ".T&\n".
	    "Lb Lb Lb Lb\n".
	    "L L L L.\n".
	    join(' % ', ('imp.\ pres.', 
                         'part.\ pres.', 
                         'part.\ past', 
                         '')), 
	    "\n";
	
	printf MAN "%s %% %s %% %s %% \n", 
	$conj->{imppre}->{'2s'}, 
	$conj->{parpre}, 
	$conj->{parpas}->{'ms'};
	
	printf MAN "%s %% %% %s %% \n", 
	$conj->{imppre}->{'1p'}, 
	$conj->{parpas}->{'mp'};
	
	printf MAN "%s %% %% %s %% \n", 
	$conj->{imppre}->{'2p'}, 
	$conj->{parpas}->{'fs'};

	printf MAN " %% %% %s %% \n", 
	$conj->{parpas}->{'fp'};
    }
    elsif ($lang eq 'it')
    {
	print MAN 
	    ".T&\n".
	    "Lb Lb Lb Lb\n".
	    "L L L L.\n".
	    join(' % ', ('imp.\ pres.', 
			 'part.\ pres.', 
			 'part.\ past', 
			 'gerund\ pres.')), 
	    "\n";

	printf MAN "%s %% %s %% %s %% %s \n", 
	$conj->{imppre}->{'2s'}, 
	$conj->{parpre}, 
	$conj->{parpas}->{'ms'},
	$conj->{gerpre};
	
	printf MAN "%s %% %% %s %% \n", 
	$conj->{imppre}->{'3s'}, 
	$conj->{parpas}->{'mp'};
	
	printf MAN "%s %% %% %s %% \n", 
	$conj->{imppre}->{'1p'}, 
	$conj->{parpas}->{'fs'};
	
	printf MAN "%s %% %% %s %% \n",
	$conj->{imppre}->{'2p'}, 
	$conj->{parpas}->{'fp'};
	
	printf MAN "%s %% %% %% \n",
	$conj->{imppre}->{'3p'};
    }    

    print MAN "% % % %\n";

    close MAN;
}

# input is an infinitive, output is a hash of 
# french-conjugator output -- this is simple but 
# verbose

sub conjugate
{
    my ($inf, $lang) = @_;

    print $fci "$inf\n";

    # print "$inf ";

    my $n = 0;
    my $C;

    # infinitive

    if (read_chomped($fco) eq "-")
    {
	return $C;
    }

    # main tenses
    
    <$fco>;

    foreach my $tense ('indpre', 'indimp', 'indfut', 'indpas', 
		       'conpre', 'subpre', 'subimp')
    {
	<$fco>;

	foreach my $perno ('1s', '2s', '3s', '1p', '2p', '3p')
	{
	    $C->{$tense}->{$perno} = split_compound($fco);
	}
    }

    # imperative present

    <$fco>;
    if ($lang eq 'it')
    {
	$C->{imppre}->{'2s'} = read_chomped($fco);
	$C->{imppre}->{'3s'} = read_chomped($fco);
	$C->{imppre}->{'1p'} = read_chomped($fco);
	$C->{imppre}->{'2p'} = read_chomped($fco);
	$C->{imppre}->{'3p'} = read_chomped($fco);
    }
    elsif ($lang eq 'fr')
    {
	$C->{imppre}->{'2s'} = read_chomped($fco);
	$C->{imppre}->{'1p'} = read_chomped($fco);
	$C->{imppre}->{'2p'} = read_chomped($fco);
    }
    else
    {
	die "no such lang $lang";
    }

    # participle present

    <$fco>;
    $C->{parpre} = read_chomped($fco);
    
    # participle past

    <$fco>;
    $C->{parpas}->{ms} = read_chomped($fco);
    $C->{parpas}->{mp} = read_chomped($fco);
    $C->{parpas}->{fs} = read_chomped($fco);
    $C->{parpas}->{fp} = read_chomped($fco);

    # gerund present

    if ($lang eq 'it')
    {
	<$fco>;
	$C->{gerpre} = read_chomped($fco);
    }

    <$fco>;

    return $C;
}

sub split_compound
{
    my $fco = shift;

    my $c = read_chomped($fco);

    my ($p, $f) = ($c =~ /^(.*?)([^' ]+)$/o);

    return {
	pronoun => $p, 
	form    => $f,
    };
}

sub read_chomped
{
    my $fh = shift;

    my $line = <$fh>;
    chomp $line;

    return $line;
}
