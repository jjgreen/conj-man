conj-man
--------

This is the conj-man package, a collection of UTF8 Unix manual
pages of conjugations of popular French and Italian verbs.
The list of French infinitives and their English translations
is a modified version of the list by [Will Dudziak][1]; the
conjugations are from the Verbiste program by [Pierre Sarrazin][2].

All files in the package are released under the GNU General Public
License (GPL), see the file COPYING for details.

J.J.Green 2015 (j.j.green@gmx.com)

[1]: http://www.dudziak.com/verbs.php
[2]: http://sarrazip.com/dev/verbiste.html
