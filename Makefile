# Makefile for conj-man
# J.J. Green 2010, 2014

default :

RUBBISH += config.log config.status Common.mk

build : 
	$(MAKE) -C src build

clean :
	$(MAKE) -C src clean
	$(RM) $(RUBBISH)

veryclean spotless :
	$(MAKE) -C src veryclean
	$(RM) -r autom4te.cache 
	$(RM) $(RUBBISH)

include Common.mk

